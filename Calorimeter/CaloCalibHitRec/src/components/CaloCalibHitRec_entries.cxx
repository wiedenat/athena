#include "CaloCalibHitRec/CalibHitToCaloCell.h"
#include "CaloCalibHitRec/CalibHitIDCheck.h"
#include "CaloCalibHitRec/CaloCalibClusterMomentsMaker.h"
#include "CaloCalibHitRec/CaloCalibClusterMomentsMaker2.h"


DECLARE_COMPONENT( CalibHitToCaloCell )
DECLARE_COMPONENT( CalibHitIDCheck )

DECLARE_COMPONENT( CaloCalibClusterMomentsMaker )
DECLARE_COMPONENT( CaloCalibClusterMomentsMaker2 )

